<section id="about" class="about">
    <div class="container" data-aos="fade-up">

      <div class="section-header">
        <h2>Program Studi Pengelolaan Perkebunan</h2>
        <h2>Visi dan Misi</h2>
        {{-- <h3>Visi</h3>
        <p>Menjadi Program Studi unggulan penyelenggara pendidikan sarjana sains terapan dalam bidang manajemen perkebunan yang menunjang pembangunan perkebunan berstandar internasional</p> --}}
      </div>

      <div class="row gy-4">
        <div class="col-lg-6">
          <h3>Visi</h3>
          <p>Menjadi Program Studi unggulan penyelenggara pendidikan sarjana sains terapan dalam bidang manajemen perkebunan yang menunjang pembangunan perkebunan berstandar internasional</p>
          <h3>Misi</h3>
          <img src="assets/img/about.jpg" class="img-fluid rounded-4 mb-4" alt="">
          <p>Menyelenggarakan pendidikan tinggi vokasional pada Diploma IV pada bidang manajemen perkebunan.</p>
          <p>Menyelenggarakan proses pendidikan yang berorientasi kepada sistem manajemen mutu kearah GAP (Good Agriculture Practices), GHP (Good Handling Practices) dan GMP (Good Manufacturing Practices).</p>
        </div>
        <div class="col-lg-6">
          <div class="content ps-0 ps-lg-5">
            <h3>Tujuan</h3>
            <p class="fst-italic">
              Menghasilkan lulusan yang menguasai kemampuan professional dalam mengelola pekerjaan yang kompleks dalam usaha perkebunan.
            </p>
            <h3>Sasaran dan Strategi</h3>
            <ul>
              <li><i class="bi bi-check-circle-fill"></i> Terselenggaranya pendidikan manajemen perkebunan yang berbasis GAP (Good Agriculture Practice) dan berorientasi interpreneurship.</li>
              <li><i class="bi bi-check-circle-fill"></i> Terselenggaranya pengembangan iptek terapan melalui penelitian dan pengabdian kepada masyarakat.</li>
              <li><i class="bi bi-check-circle-fill"></i> Terselenggaranya kerjasama dengan stakehoulders.</li>
              <li><i class="bi bi-check-circle-fill"></i> Terselenggaranya uji kompetensi yang bersertifikasi</li>
            </ul>
            {{-- <p>
              Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
              velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident
            </p> --}}

            {{-- <div class="position-relative mt-4">
              <img src="assets/img/about-2.jpg" class="img-fluid rounded-4" alt="">
              <a href="https://www.youtube.com/watch?v=LXb3EKWsInQ" class="glightbox play-btn"></a>
            </div> --}}
          </div>
        </div>
      </div>

    </div>
  </section>