<section id="team" class="team">
    <div class="container" data-aos="fade-up">
      <div class="section-header">
        <h2>Dosen dan Tenaga Kependidikan</h2>
        <p>Berikut merupakan daftar Dosen dan Tenaga Kependidikan yang mengajar di Prodi Pengelolaan Perkebunan</p>
      </div>

      <div class="row gy-4">
        @foreach ($teams as $team)
            {{-- {{ dd($t?eam)  }} --}}
        <div class="col-xl-3 col-md-6 d-flex" data-aos="fade-up" data-aos-delay="100">
          <div class="member">
            <img src="{{   $team->getImage() }}" class="img-fluid" alt="">
            <h4>{{  $team->name  }}</h4>
            <span>{{ $team->position }}</span>
            <div class="social">
              <a href=""><i class="bi bi-twitter"></i></a>
              <a href=""><i class="bi bi-facebook"></i></a>
              <a href=""><i class="bi bi-instagram"></i></a>
              <a href=""><i class="bi bi-linkedin"></i></a>
            </div>
          </div>

        </div><!-- End Team Member -->
        @endforeach


      </div>

    </div>
  </section>