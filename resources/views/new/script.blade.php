
  <!-- Vendor JS Files -->
  <script src="{{ asset('new/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('new/assets/vendor/aos/aos.js') }}"></script>
  <script src="{{ asset('new/assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
  <script src="{{ asset('new/assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
  <script src="{{ asset('new/assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
  <script src="{{ asset('new/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('new/assets/vendor/php-email-form/validate.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('new/assets/js/main.js') }}"></script>
  @stack('script')
