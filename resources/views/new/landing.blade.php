@extends('new.index')
@section('content')
<!-- ======= About Us Section ======= -->
@include('new.call')
<!-- End About Us Section -->

<!-- ======= Clients Section ======= -->
@include('new.client')
<!-- End Clients Section -->

<!-- ======= Stats Counter Section ======= -->
@include('new.stats')
<!-- End Stats Counter Section -->

<!-- ======= Call To Action Section ======= -->
@include('new.about')
<!-- End Call To Action Section -->

<!-- ======= Our Services Section ======= -->
@include('new.services')
<!-- End Our Services Section -->

<!-- ======= Testimonials Section ======= -->
{{-- @include('new.testimonial') --}}
<!-- End Testimonials Section -->

<!-- ======= Portfolio Section ======= -->
{{-- @include('new.portofolio') --}}
<!-- End Portfolio Section -->

<!-- ======= Our Team Section ======= -->
@include('new.team')
<!-- End Our Team Section -->

<!-- ======= Pricing Section ======= -->
{{-- @include('new.pricing')   --}}
<!-- End Pricing Section -->

<!-- ======= Frequently Asked Questions Section ======= -->
{{-- @include('new.faq') --}}
<!-- End Frequently Asked Questions Section -->

<!-- ======= Recent Blog Posts Section ======= -->
{{-- @include('new.recentblog') --}}
<!-- End Recent Blog Posts Section -->

<!-- ======= Contact Section ======= -->
{{-- @include('new.contact') --}}
<!-- End Contact Section -->
@endsection