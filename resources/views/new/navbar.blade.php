<nav id="navbar" class="navbar">
    <ul>
      <li class="dropdown"><a href="#"><span>Profil</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
        <ul>
          <li><a href="#">Selayang Pandang</a></li>
            <ul>
              <li><a href="#">Deep Drop Down 1</a></li>
              <li><a href="#">Deep Drop Down 2</a></li>
              <li><a href="#">Deep Drop Down 3</a></li>
              <li><a href="#">Deep Drop Down 4</a></li>
              <li><a href="#">Deep Drop Down 5</a></li>
            </ul>
          </li>
          <li><a href="{{ route('site.vision') }}">Visi & Misi</a></li>
          {{-- <li><a href="{{ route('site.organization') }}">Struktur Organisasi</a></li> --}}
          <li><a href="{{ route('site.teams.lecturer') }}">Dosen</a></li>
          <li><a href="{{ route('site.teams.staff') }}">Tenaga Kependidikan</a></li>
          <li><a href="{{ route('site.partner') }}">Kerjasama</a></li>
          <li><a href="{{ route('site.acreditation') }}">Akreditasi</a></li>
        </ul>
        <li class="dropdown"><a href="#"><span>Akademik</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
          <ul>
            <li><a href="{{ route('site.documents') }}">Direktori Akademik</a></li>
              <ul>
                <li><a href="#">Deep Drop Down 1</a></li>
                <li><a href="#">Deep Drop Down 2</a></li>
                <li><a href="#">Deep Drop Down 3</a></li>
                <li><a href="#">Deep Drop Down 4</a></li>
                <li><a href="#">Deep Drop Down 5</a></li>
              </ul>
            </li>
            <li><a href="{{ route('site.job') }}">Profil Lulusan</a></li>
            <li><a href="{{ route('site.tuition') }}">Biaya Kuliah</a></li>
            <li><a href="{{ route('site.service') }}">Layanan Politani</a></li>
            <li><a href="{{ route('site.curriculum') }}">Kurikulum</a></li>
            <li><a href="{{ route('site.graduation') }}">Syarat Kelulusan</a></li>
            <li><a href="{{ route('site.leave') }}">Cuti Akademik</a></li>
            <li><a href="{{ route('site.repository') }}">Panduan Upload Repository</a></li>
          </ul>
      <li><a href="{{ route('site.research') }}">Riset & Pengabdian</a></li>
      <li><a href="{{ route('site.announcement') }}">Pengumuman</a></li>
      <li class="dropdown"><a href="#"><span>Unit Pendukung</span> <i class="bi bi-chevron-down dropdown-indicator"></i></a>
        <ul>
          <li><a href="{{ route('site.gallery') }}">Galeri Laboratorium</a></li>
            <ul>
              <li><a href="#">Deep Drop Down 1</a></li>
              <li><a href="#">Deep Drop Down 2</a></li>
              <li><a href="#">Deep Drop Down 3</a></li>
              <li><a href="#">Deep Drop Down 4</a></li>
              <li><a href="#">Deep Drop Down 5</a></li>
            </ul>
          </li>
          <li><a href="{{ route('site.organization') }}">Organisasi Mahasiswa</a></li>
        </ul>
      <li><a href="{{ route('site.blogs') }}">Artikel</a></li>
      </li>
      <li><a href="{{ route('site.contact') }}">Contact</a></li>
    </ul>
  </nav><!-- .navbar -->