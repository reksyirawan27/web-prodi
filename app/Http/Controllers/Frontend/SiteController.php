<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Academic;
use App\Models\Announcement;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Curriculum;
use App\Models\Document;
use App\Models\Faq;
use App\Models\Filter;
use App\Models\Gallery;
use App\Models\Graduation;
use App\Models\Leave;
use App\Models\Organization;
use App\Models\Outdoor;
use App\Models\Partner;
use App\Models\Program;
use App\Models\Repository;
use App\Models\Research;
use App\Models\Scholarship;
use App\Models\Slider;
use App\Models\Team;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        // $cat_id = Category::where('slug', 'pengumuman')->first();
        $sliders = Slider::orderBy('created_at', 'desc')->get();
        $blogs = Blog::orderBy('created_at', 'desc')->take(3)->get();
        $teams = Team::orderBy('position', 'desc')->get();
        return view('new.landing', compact('blogs', 'sliders', 'teams'));
    }

    public function contact()
    {
        $contact = Contact::first();
        return view('new.contact', compact('contact'));
    }

    public function teamsLecturer()
    {
        $teams = Team::orderBy('name', 'asc')->where('position', '=', 'Dosen')->get();
        return view('new.profil.dosen', compact('teams'));
    }

    public function teamsStaff()
    {
        $teams = Team::orderBy('position', 'asc')->where('position', '!=', 'Dosen')->get();
        return view('new.profil.tendik', compact('teams'));
    }

    public function teamDetail($id)
    {
        $team = Team::find($id);
        return view('front.teamDetail', compact('team'));
    }

    public function teamStaffDetail($id)
    {
        $team = Team::find($id);
        return view('front.teamStaffDetail', compact('team'));
    }

    public function documents()
    {
        $documents = Document::orderBy('created_at', 'desc')->paginate(10);
        return view('new.akademik.direktori', compact('documents'));
    }

    public function datatables()
    {
        $documents = Document::orderBy('created_at', 'desc')->get();
        return datatables()->of($documents)
            ->addColumn('file', function (Document $document) {
                return '<a href="' . $document->getFile() . '" />' . $document->file . '</a>';
            })
            ->addIndexColumn()
            ->rawColumns(['file'])
            ->toJson();
    }

    public function blogs()
    {
        $categories = Category::all();
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(4);
        return view('new.blog', compact('blogs', 'categories'));
    }

    public function blogDetail($slug)
    {
        $categories = Category::all();
        $blog = Blog::where('slug', '=', $slug)->first();
        return view('front.blogDetail', compact('blog', 'categories'));
    }

    public function blogFilterCategory($id)
    {
        $category = Category::find($id);
        $blogs = Blog::orderBy('created_at', 'desc')->paginate(5);
        return view('front.blogFilterCategory', compact('blogs', 'category'));
    }

    public function performance()
    {
        $cat_id = Category::where('slug', 'prestasi')->first();
        $performances = Blog::orderBy('created_at', 'desc')->paginate(5);
        return view('new.prestasi', compact('performances', 'cat_id'));
    }

    public function announcement()
    {
        $announcements = Announcement::orderBy('created_at', 'desc')->paginate(5);
        return view('new.pengumuman', compact('announcements'));
    }

    public function announcementDetail($slug)
    {
        $announcement = Announcement::where('slug', '=', $slug)->first();
        $anothers = Announcement::orderBy('created_at', 'desc')->where('id', '!=', $announcement->id)->take(5)->get();
        return view('front.announcementDetail', compact('announcement', 'anothers'));
    }

    public function job()
    {
        $job = Academic::whereNotNull('job')->first();
        return view('new.akademik.lulusan', compact('job'));
    }

    public function tuition()
    {
        $tuition = Academic::whereNotNull('tuition')->first();
        return view('new.akademik.biaya', compact('tuition'));
    }

    public function service()
    {
        $service = Academic::whereNotNull('service')->first();
        return view('new.akademik.layanan', compact('service'));
    }

    public function faq()
    {
        $faqs = Faq::orderBy('created_at', 'asc')->get();
        return view('new.faq', compact('faqs'));
    }

    public function story()
    {
        $story = Program::whereNotNull('story')->first();
        return view('front.story', compact('story'));
    }

    public function vision()
    {
        $vision = Program::whereNotNull('vision')->first();
        return view('new.profil.visi-misi', compact('vision'));
    }

    public function structure()
    {
        $structure = Program::whereNotNull('structure')->first();
        return view('front.structure', compact('structure'));
    }

    public function acreditation()
    {
        $acreditation = Program::whereNotNull('acreditation')->first();
        return view('new.profil.akreditas', compact('acreditation'));
    }

    public function curriculum()
    {
        $curriculums = Curriculum::orderBy('code', 'asc')->get()->groupBy('semester');
        return view('new.akademik.kurikulum', compact('curriculums'));
    }

    public function gallery()
    {
        $filters = Filter::all();
        $galleries = Gallery::all();

        return view('new.unitpendukung.galeri', compact('filters', 'galleries'));
    }

    public function partner()
    {
        $edupartners = Partner::orderBy('created_at', 'desc')->where('edu', '=', 'yes')->get();
        $partners = Partner::orderBy('created_at', 'desc')->whereNull('edu')->paginate(10);
        return view('new.profil.kerjasama', compact('edupartners', 'partners'));
    }

    public function scholarship()
    {
        $scholarship = Scholarship::whereNotNull('scholarship')->first();
        return view('new.beasiswa', compact('scholarship'));
    }

    public function organization()
    {
        $organization = Organization::whereNotNull('organization')->first();
        return view('new.unitpendukung.organisasi', compact('organization'));
    }

    public function research()
    {
        $research = Research::whereNotNull('research')->first();
        return view('new.reset', compact('research'));
    }

    public function graduation()
    {
        $graduation = Graduation::whereNotNull('graduation')->first();
        return view('new.akademik.syarat', compact('graduation'));
    }

    public function leave()
    {
        $leave = Leave::whereNotNull('leave')->first();
        return view('new.akademik.cuti', compact('leave'));
    }

    public function repository()
    {
        $repository = Repository::whereNotNull('repository')->first();
        return view('new.akademik.panduan', compact('repository'));
    }
}
